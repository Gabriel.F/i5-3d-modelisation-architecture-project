// Collapsible sub-menus
let coll = document.getElementsByClassName("collapsible");
let i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    let content = this.nextElementSibling;
    if (content.style.display === "none") {
      content.style.display = "block";
    } else {
      content.style.display = "none";
    }
  });
}

// Constants
const MAIN_SCENE_BACKGROUND_COLOR = 0x42a1f4; // Blue
const NEW_MESHES_BACKGROUND_COLOR = 0xf4df42; // Yellow
const NEW_MESHES_COLOR = 0xadb7c9; // Grey
const ANIMATION_SPEED = 0.02;

// Renderers creation
let sceneCanvas = document.getElementById('scene');
let model1Canvas = document.getElementById('menu-model-1-scene');
let model2Canvas = document.getElementById('menu-model-2-scene');
let model3Canvas = document.getElementById('menu-model-3-scene');

let renderer = new THREE.WebGLRenderer({ canvas: sceneCanvas });
let model1Renderer = new THREE.WebGLRenderer({ canvas: model1Canvas });
let model2Renderer = new THREE.WebGLRenderer({ canvas: model2Canvas });
let model3Renderer = new THREE.WebGLRenderer({ canvas: model3Canvas });

renderer.setSize(sceneCanvas.offsetWidth, sceneCanvas.offsetHeight);
model1Renderer.setSize(model1Canvas.offsetWidth, model1Canvas.offsetHeight);
model2Renderer.setSize(model2Canvas.offsetWidth, model2Canvas.offsetHeight);
model3Renderer.setSize(model3Canvas.offsetWidth, model3Canvas.offsetHeight);

// Cameras creation
let camera = new THREE.PerspectiveCamera(45, sceneCanvas.offsetWidth / sceneCanvas.offsetHeight, 0.5, 1000);
camera.position.z = 10;

let model1Camera = new THREE.PerspectiveCamera(45, model1Canvas.offsetWidth / model1Canvas.offsetHeight, 0.5, 1000);
model1Camera.position.z = 3;

let model2Camera = new THREE.PerspectiveCamera(45, model2Canvas.offsetWidth / model2Canvas.offsetHeight, 0.5, 1000);
model2Camera.position.z = 3;

let model3Camera = new THREE.PerspectiveCamera(45, model3Canvas.offsetWidth / model3Canvas.offsetHeight, 0.5, 1000);
model3Camera.position.z = 3;

// Scenes creation
let scene = new THREE.Scene();
scene.background = new THREE.Color(MAIN_SCENE_BACKGROUND_COLOR);

let model1Scene = new THREE.Scene();
model1Scene.background = new THREE.Color(NEW_MESHES_BACKGROUND_COLOR);

let model2Scene = new THREE.Scene();
model2Scene.background = new THREE.Color(NEW_MESHES_BACKGROUND_COLOR);

let model3Scene = new THREE.Scene();
model3Scene.background = new THREE.Color(NEW_MESHES_BACKGROUND_COLOR);

// Groups creation
let houseGroup = new THREE.Group();
scene.add(houseGroup);

// Geometries and meshes creation
let geometry = new THREE.BoxGeometry(3,1,2, 32, 32, 32);
let planeGeometry = new THREE.PlaneGeometry(10, 10, 64, 64);
let planeMaterial = new THREE.MeshBasicMaterial({ color: 0x68837A});
let plane = new THREE.Mesh(planeGeometry, planeMaterial);

let model1Mesh = new THREE.Mesh(
    new THREE.CubeGeometry(),
    new THREE.MeshBasicMaterial({ color: NEW_MESHES_COLOR})
);
model1Scene.add(model1Mesh);

let model2Mesh = new THREE.Mesh(
    new THREE.SphereGeometry(0,0,32,32),
    new THREE.MeshBasicMaterial({ color: NEW_MESHES_COLOR})
);
model2Scene.add(model2Mesh);

let objectLoader = new THREE.OBJLoader();
let model3Mesh;
objectLoader.load(
    "models/chair.obj",
	function (object) {
        // "Re-center" object at the origin of the scene & setting up material
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh)  {
                child.geometry.computeBoundingBox();
                child.geometry.center();
                child.material = new THREE.MeshBasicMaterial({ color: NEW_MESHES_COLOR});
                model3Mesh = child;
            }
        });
        model3Mesh.rotateY(2/3 * Math.PI);
        model3Scene.add(model3Mesh);

        animate();
	},
	undefined,
	function (error) {
		console.error('An error happened while loading an OBJ file : ' + error.toString());
	}
);

let textureLoader = new THREE.TextureLoader();
let texture = textureLoader.load('textures/grass.jpg', function(texture) {
    // Setting anisotropy to the maximum level (x16 ?) to avoid the texture being blurred in narrow view angles
    texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
    plane.material.map = texture;
    plane.material.needsUpdate = true;
});
plane.rotateX( - Math.PI / 2);

houseGroup.add(plane);

let materialsArray = [
    new THREE.MeshBasicMaterial({
        color: 'brown',
        side: THREE.BackSide
    }),
    new THREE.MeshBasicMaterial({
        color: 'green',
        side: THREE.BackSide
    }),
    new THREE.MeshBasicMaterial({
        color: 'red',
        side: THREE.BackSide
    }),
    new THREE.MeshBasicMaterial({
        color: 'blue',
        side: THREE.BackSide
    }),
    new THREE.MeshBasicMaterial({
        color: 'purple',
        side: THREE.BackSide
    }),
    new THREE.MeshBasicMaterial({
        color: 'orange',
        side: THREE.BackSide
    })
];

// Mesh
let mesh = new THREE.Mesh(geometry, materialsArray);
houseGroup.add(mesh);
mesh.position.set(0, 0.51, 0);

// Display contour
let edgeHelper = new THREE.EdgesHelper( mesh, 0xffffff);
edgeHelper.position.set(0, 0.51, 0)
houseGroup.add(edgeHelper);

 // Lights creation
let light = new THREE.DirectionalLight();
light.position.set(2,2,2);

scene.add(light);
model1Scene.add(light);
model2Scene.add(light);
model3Scene.add(light);

// Orbit control
let orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
orbitControls.minDistance = 2;
orbitControls.maxDistance = 30;

// Transform control
let transformControls = new THREE.TransformControls(camera, renderer.domElement);
scene.add(transformControls);

// On resize
/* function onWindowResize() {
    camera.aspect = sceneCanvas.offsetWidth / sceneCanvas.offsetHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(sceneCanvas.offsetWidth, sceneCanvas.offsetHeight);
    renderer.render(scene, camera);
} */

// Animation
function animate() {
    orbitControls.update();

    model1Mesh.rotateY(0.02);
    model2Mesh.rotateY(0.02);
    model3Mesh.rotateY(0.02);
    
    renderer.render(scene, camera);
    model1Renderer.render(model1Scene, model1Camera);
    model2Renderer.render(model2Scene, model2Camera);
    model3Renderer.render(model3Scene, model3Camera);

    requestAnimationFrame(animate);
}

/******************************
********* RAY CASTING ********* 
******************************/

let raycaster = new THREE.Raycaster();
let mouse = new THREE.Vector2();

function getFirstEncounter(position) {
    raycaster.setFromCamera(position, camera);
    let selected = raycaster.intersectObjects(scene.children);
    if (selected.length) {
        return selected[0].object;
    }
}

function onMouseClick(event) {
    let position = new THREE.Vector2();
    let domRect = renderer.domElement.getBoundingClientRect();

    position.x = (event.clientX / domRect.width) * 2 - 1 + domRect.left;
    position.y = - (event.clientY / domRect.height) * 2 + 1 + domRect.top;

    let s = getFirstEncounter(position);
    if (s) {
        console.log("Vous avez sélectionné l'objet " + s.name);
        transformControls.attach(s);
    } else {
        console.log("Vous n'avez rien sélectionné");
    };
  }

/*************************************
********* EVENTS DECLARATION ********* 
*************************************/

renderer.domElement.addEventListener('click', onMouseClick);

// Disables orbit controls while doing operations on a mesh
transformControls.addEventListener('dragging-changed', function(event) {
    orbitControls.enabled = ! event.value;
});

// Changes transform controls mode
window.addEventListener('keydown', function(event) {
    switch (event.keyCode) {
        // ESC
        case 27:
            transformControls.enabled = !transformControls.enabled;
            break;
        // R
        case 82:
            transformControls.setMode( "rotate" );
            break;
        // S
        case 83: 
            transformControls.setMode( "scale" );
            break;
        // T
        case 84:
            transformControls.setMode( "translate" );
            break;
        // X
        case 88:
            transformControls.showX = !transformControls.showX;
            transformControls.showY = !transformControls.showY;
            transformControls.showZ = !transformControls.showZ;
            break;
    } 
});

/* window.addEventListener('resize', onWindowResize, false); */
// window.addEventListener('mousedown', onDocumentMouseDown, false);

function addMeshToMainScene(mesh) {
    newMesh = mesh.clone();
    newMesh.position.set(0, 1, 0);
    scene.add(newMesh);
    transformControls.setMode("translate");
    transformControls.attach(newMesh);
}

// Button events
document.getElementById('add-cube').addEventListener('click', function() {
    addMeshToMainScene(model1Mesh);
});
document.getElementById('add-sphere').addEventListener('click', function() {
    addMeshToMainScene(model2Mesh);
});
document.getElementById('add-chair').addEventListener('click', function() {
    addMeshToMainScene(model3Mesh);
});