# Projet I5 - Modélisation 3D

## Sujet
Le but est de créer une sorte de mini logiciel d'architecture, où on aurait une scène et où on pourrait déposer en drag-n-drop des objets parmi ceux proposés.

## Instructions de lancement
* Exécuter le fichier "python-webserver.bat" qui lancera un serveur Web Python qui servira nos différents fichiers
* Lancer "main.html" avec Chrome (recommandé pour sa bonne prise en charge et ses performances WebGL)

## Caractéristiques du MVP
* Maison modélisée et vide
* Interface de drag'n'drop sur la droite avec des images des modèles en légère rotation
* Axes disponibles pour la translation des objets
* Axes disponibles pour la rotation des objets
* Drag'n'drop fonctionnel

## Idées d'approfondissement
* Utiliser des touches pour activer la translation/rotation et les afficher sur l'UI
* Mettre des contours sur l'objet sélectionné afin d'avoir un repaire visuel
* Peaufiner interface
* Positionner par défaut la caméra dans une position plus "naturelle"
* Ajouter gravité
* Ajouter système de collision avec les murs
* Modélisation plus précise de la maison
* Mettre les contours de la face non visible en fil de fer
* Ajouter des objets depuis des fichiers 3D
* Technique : refacto en TypeScript

## Notes techniques
Le fichier `typings.json` et le dossier `typings` permettent d'avoir de l'autocomplétion pour ThreeJS sur Visual Studio Code. 
Les fichiers ont été poussés sur le repo afin d'éviter à refaire certaines manipulations en local.